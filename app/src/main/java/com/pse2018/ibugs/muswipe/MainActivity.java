/*
 * @file MainActivity.java
 * @brief Main Activity of the app.
 *
 * @author Federico Turrin (<federico.turrin@studenti.unipd.it>)
 * @author Marco Tramarin (<marco.tramarin@studenti.unipd.it>)
 * @author Alessandro Pomes (<alessandro.pomes@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2016-2017 University of Padua, Italy
 * @copyright Apache License, Version 2.0
 */

package com.pse2018.ibugs.muswipe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{
    // Variables.
    Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize the button to start DrawActivity.
        mButton = findViewById(R.id.but);
        mButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent toDraw=new Intent(MainActivity.this, DrawActivity.class);
                startActivity(toDraw);
            }
        });
    }
}
