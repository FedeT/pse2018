/*
 * @file MainActivity.java
 * @brief This is the second activity. It allows to draw gestures.
 *
 * @author Federico Turrin (<federico.turrin@studenti.unipd.it>)
 * @author Marco Tramarin (<marco.tramarin@studenti.unipd.it>)
 * @author Alessandro Pomes (<alessandro.pomes@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2016-2017 University of Padua, Italy
 * @copyright Apache License, Version 2.0
 */

package com.pse2018.ibugs.muswipe;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;

public class DrawActivity extends AppCompatActivity
{
    //Variables.
    TextView text;                                       // Eventual text from previous activity.

    Button restart;
    Button next;

    Drawing frag;                                        // Fragment definition.

    ArrayList<Integer> gest = new ArrayList<>();         // Detected gestures.

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Interface initialization.
        setContentView(R.layout.activity_draw);
        restart = findViewById(R.id.reset);
        next = findViewById(R.id.cont);

        // Restore previous fragment if present.
        if(savedInstanceState != null && savedInstanceState.containsKey("frag"))
        {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            frag = (Drawing) getFragmentManager().getFragment(savedInstanceState,"frag");
            transaction.replace(R.id.frameLayout, frag); // fragment container id in first parameter is the  container(Main layout id) of Activity
            transaction.commit();
        }
        // Create new fragment.
        else
        {
            frag = new Drawing();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.frameLayout, frag); // fragment container id in first parameter is the  container(Main layout id) of Activity
            transaction.commit();
        }

        // Restart activity in case of restart button pression.
        restart.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Create new Fragment.
                frag = new Drawing();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frameLayout, frag); // fragment container id in first parameter is the  container(Main layout id) of Activity
                transaction.commit();
            }
        });

        // Launch the next activity and pass the arraylist of gestures.
        next.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // 1- Set loading animation visible.
                findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);

                // 2- Disable touch event while loading.
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                // 3- Call Generator.
                gest = new ArrayList<>(frag.gests);
                Generator gen = new Generator(gest, getApplicationContext());
                gen.generate();

                // 4- Start transition to PlayActivity.
                Intent toPlay = new Intent(DrawActivity.this, PlayActivity.class);

                // 5- Pass parameters to PlayActivity in form (key, value)
                toPlay.putExtra("samples",gen.getSamples());
                startActivity(toPlay);
            }
        });

    }

    // Save fragment state.
    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        // Put actual fragment.
        getFragmentManager().putFragment(outState,"frag",frag);
    }

    //Hide animation loading when activity is restarted
    @Override
    public void onRestart()
    {
        super.onRestart();
        // Hide loading animation.
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        // Reactivate touch event again.
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
