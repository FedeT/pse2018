/*
 * @file MainActivity.java
 * @brief Fragment for the gesture draw and detection implementation.
 *
 * @author Federico Turrin (<federico.turrin@studenti.unipd.it>)
 * @author Marco Tramarin (<marco.tramarin@studenti.unipd.it>)
 * @author Alessandro Pomes (<alessandro.pomes@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2016-2017 University of Padua, Italy
 * @copyright Apache License, Version 2.0
 */

package com.pse2018.ibugs.muswipe;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;

public class Drawing extends Fragment
{
    // Variables.
    public GestureRecognizer recog;
    public ArrayList<GestureRecognizer.Point> list = new ArrayList<>();  // List of points detected.
    public ArrayList<Integer> gests = new ArrayList<>();                // List of detected gestures.
    public GestureRecognizer.Result result;

    TextView text;                                                  // Result text.
    public DrawOnTouchView draw;
    public ArrayList<MotionEvent> eventList = new ArrayList<>();    // Save event for restoring.

    // Add the frame view.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.activity_frame, container, false);
        draw = new DrawOnTouchView(getContext(), null);
        rootView.addView(draw);
        return rootView;
    }

    // Set text on View.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        text = Objects.requireNonNull(getView()).findViewById(R.id.textFrame);

        // Restore previous state if present.
        if (savedInstanceState != null)
        {
            ArrayList<MotionEvent> eventL = savedInstanceState.getParcelableArrayList("event_list");
            // Repeat for every event.
            for(MotionEvent event: Objects.requireNonNull(eventL))
            {   draw.onTouchEvent(event); }
        }
    }


    /**
     * Class for the gestures management.
     */
    public class DrawOnTouchView extends View
    {
        // Variables.
        private Paint paint = new Paint();
        private Path path = new Path();
        Context context;
        float eventX;
        float eventY;

        GestureDetector gestureDetector;

        // Constructor.
        public DrawOnTouchView(Context context, AttributeSet attrs)
        {
            super(context, attrs);
            gestureDetector = new GestureDetector(context, new GestureListener());
            this.context = context;

            // Draw attribute settings.
            paint.setAntiAlias(true);
            paint.setStrokeWidth(6f);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
        }

        // Gesture listener class.
        private class GestureListener extends GestureDetector.SimpleOnGestureListener
        {
            @Override
            public boolean onDoubleTap(MotionEvent e)
            {   return true;   }
        }

        /**
         * Allow to draw on the screen.
         * @param canvas Canvas to draw
         **/
        @Override
        protected void onDraw(Canvas canvas)
        {   canvas.drawPath(path, paint);  }

        /**
         * Method for gestures recording and identification.
         * @param event Event detected
         **/
        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            // 1- Record the points.
            eventX = event.getX();
            eventY = event.getY();
            eventList.add(MotionEvent.obtain(event));

            // 2- Invoke gesture detector.
            gestureDetector.onTouchEvent(event);

            switch (event.getAction())
            {
                // Click event.
                case MotionEvent.ACTION_DOWN:
                    // Store the coordinates in the list.
                    list.add(new GestureRecognizer.Point(eventX,eventY));
                    // Draw the event
                    path.moveTo(eventX, eventY);
                    return true;

                // Swipe event.
                case MotionEvent.ACTION_MOVE:
                    // Store the coordinates in the list.
                    list.add(new GestureRecognizer.Point(eventX,eventY));
                    // Draw the event
                    path.lineTo(eventX, eventY);
                    break;

                // Stop recording event.
                case MotionEvent.ACTION_UP:
                    // Draw the event
                    path.moveTo(eventX, eventY);
                    //Delegate operations to performClick
                    performClick();
                    break;

                default:
                    return false;
            }

            invalidate();
            return true;
        }

        /*

         */
        @Override
        public boolean performClick() {
            // Calls the super implementation, which generates an AccessibilityEvent
            // and calls the onClick() listener on the view, if any
            super.performClick();
            // Handle the action for the custom click here
            // 1- Store the coordinates in the list.
            list.add(new GestureRecognizer.Point(eventX, eventY));
            // 2- Create an instance of GestureRecognizer class with the current list of coordinates.
            recog = new GestureRecognizer(list);
            // 3- Find the shape.
            result = recog.getResult();
            // 4- Display on the screen the result.
            //Conversion in 2 decimal digits
            BigDecimal score = new BigDecimal(result.Score*100).setScale(2,BigDecimal.ROUND_HALF_UP);
            String tmpString = result.Name + " (" + score + "%)";
            text.setText(tmpString);
            // 5- Add to the list of detected gestures.
            gests.add(result.type);
            // 6- Clear the present list-
            list.clear();
            list = new ArrayList<>();
            return true;
        }
    }

    // Save state.
    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("event_list", eventList);
    }


}