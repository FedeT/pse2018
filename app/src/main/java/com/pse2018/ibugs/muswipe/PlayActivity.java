/*
 * @file PlayActivity.java
 * @brief This class read and reproduce the model generated from the Generator class.
 *
 * @author Federico Turrin (<federico.turrin@studenti.unipd.it>)
 * @author Marco Tramarin (<marco.tramarin@studenti.unipd.it>)
 * @author Alessandro Pomes (<alessandro.pomes@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2016-2017 University of Padua, Italy
 * @copyright Apache License, Version 2.0
 */
package com.pse2018.ibugs.muswipe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;

import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;

import android.util.SparseIntArray;
import java.lang.Math;
import java.util.Arrays;
import java.util.Vector;

public class PlayActivity extends AppCompatActivity
{
    // Constants.

    public static final String AUDIO = "Audio";

    public static final int FRAME_RATE = 44100;     // Sample rate.
    public static final int L = 8192;               // Look-up table length.

    public static final int MIN_NOTE = 48;          // Minimum represented note.
    public static final int MAX_NOTE = 84;          // Maximum represented note.

    public static final int TIME_RESOLUTION = 16;   // Metric unit associated to a time step.
    public static final int NUM_TIME_STEP = 16;     // Total number of time steps.
    public static final int DEN = 4;                // Fundamental metric unit of a measure.
    public static final int BPM = 90;               // Time.
    public static final int LOOP = 4;               // Number of repetitions.

    public static final int UNIT_DURATION = FRAME_RATE*60*DEN/(BPM*TIME_RESOLUTION);
    public static final int DURATION = NUM_TIME_STEP*FRAME_RATE*60*DEN/(BPM*TIME_RESOLUTION);

    // Variables.
    Button mButtonPlay;                          // Button to play audio.
    Button mButtonRedraw;                        // Button to make another draw.

    AudioTrack mAudioTrack;                      // Audio player.

    private int[] input;                                    // Input vector from the model
    private short[] mSamples = new short[DURATION];    // Vector for the audio signal.

    private static double[] mTable = new double[L];                     // Vector for the look-up table.
    private static double[] mEnvelope = new double[4*UNIT_DURATION] ;   // Vector for the envelope.

    private Vector<Vector<Integer>> mScoreOn;    // Score for note enabling.
    private Vector<Vector<Integer>> mScoreOff;   // Score for note disabling.

    // Activity methods.
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // 1- Variables initialization.
        if (savedInstanceState != null)
        {
            mSamples = savedInstanceState.getShortArray(AUDIO); }
        else
        {
            // Get the input vector generated in DrawActivity.
            input = getIntent().getIntArrayExtra("samples");
            initData();
            generateTone(mScoreOn, mScoreOff);
        }

        // 2- Interface initialization.
        setContentView(R.layout.activity_play);
        mButtonPlay = findViewById(R.id.play_button);
        mButtonRedraw = findViewById(R.id.redraw);


        // 3- Audio reproduction.
        mButtonPlay.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {   reproduce();   }
        });

        // 4- Return to DrawActivity option.
        mButtonRedraw.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent toSecond = new Intent(PlayActivity.this, DrawActivity.class);
                startActivity(toSecond);
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        mSamples = savedInstanceState.getShortArray(AUDIO);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putShortArray(AUDIO, mSamples);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if (mAudioTrack != null)
        {
            mAudioTrack.release();
            mAudioTrack = null;
        }
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (mAudioTrack != null)
        {
            mAudioTrack.release();
            mAudioTrack = null;
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (mAudioTrack != null)
        {
            mAudioTrack.release();
            mAudioTrack = null;
        }
    }

    // Private methods and classes.
    /**
     * Initialize look-up table, envelope and scores.
     */
    private void initData()
    {
        // Look-up table and envelope initialization.
        for (int i = 0; i < L; i++)                           // Look-up table.
            mTable[i] = Math.sin(2*Math.PI*i/L);

        int maxAmplitude = 4096;                              // Oscillator maximum amplitude.

        double a = (double) mEnvelope.length/8;               // Attack end in sample.
        double b = (double) 3*mEnvelope.length/4;             // Sustain end in sample.
        double c = (double) mEnvelope.length;                 // Release end in sample.
        double m1 = (double) maxAmplitude/(a);
        double m2 = (double) -maxAmplitude/(c-b);
        double q2 = (double) maxAmplitude*((mEnvelope.length-1)/(c-b));

        for (int i = 0; i < mEnvelope.length; i++)
        {
            if (i < a)
                mEnvelope[i] = m1*i;
            else if (i < b)
                mEnvelope[i] = maxAmplitude;
            else
                mEnvelope[i] = m2*i+q2;
        }

        // Scores initialization.
        mScoreOn = new Vector<>();
        mScoreOff = new Vector<>();

        int noteDiff = MAX_NOTE-MIN_NOTE;
        int rowLength = 2*noteDiff;

        Vector<Integer> noteOnNow;
        Vector<Integer> noteOffNow;

        for (int i = 0; i < NUM_TIME_STEP; i++)
        {
            noteOnNow = getNotes(Arrays.copyOfRange(input,i*rowLength,i*rowLength+noteDiff-1));
            mScoreOn.add(i, noteOnNow);
            noteOffNow = getNotes(Arrays.copyOfRange(input,i*rowLength+noteDiff,i*rowLength+rowLength-1));
            mScoreOff.add(i, noteOffNow);
        }
    }

    /**
     * Read notes in score and create a correspondent Oscillator object.
     */
    private void generateTone(Vector<Vector<Integer>> scoreOn, Vector<Vector<Integer>> scoreOff)
    {
        // Audio signal vector initialization.
        for (int j = 0; j < DURATION; j++)
            mSamples[j] = 0;

        // State variables for phase and envelope.
        SparseIntArray envList = new SparseIntArray();
        SparseIntArray phiList = new SparseIntArray();

        // Scan variables for scores.
        Vector<Integer> currStateOn;
        Vector<Integer> currStateOff;
        Vector<Integer> pitches = new Vector<>();
        Vector<Integer> prevStateOn = new Vector<>();

        for (int i = 0; i < NUM_TIME_STEP; i++)
        {
            // 1- Get notes to be played/stopped at the current time step.
            currStateOn = scoreOn.elementAt(i);
            currStateOff = scoreOff.elementAt(i);

            // 2- Copy played notes in the previous time step.
            if (!prevStateOn.isEmpty())
                pitches = prevStateOn;

            // 3- Remove notes to be stopped and the related data at the current time step.
            if (!currStateOff.isEmpty())
            {
                for (int pitch:currStateOff)
                {
                    if (pitches.contains(pitch))
                        pitches.remove(Integer.valueOf(pitch));
                    if (phiList.get(pitch) > 0)
                        phiList.delete(pitch);
                    if (envList.get(pitch) > 0)
                        envList.delete(pitch);
                }
            }

            // 4- Add notes to be played.
            if (!currStateOn.isEmpty())
                pitches.addAll(currStateOn);

            // 5- Generate notes at the current time step.
            int n = i*UNIT_DURATION;
            for (int pitch:pitches)
            {
                Oscillator osc;
                if (prevStateOn.contains(pitch))
                    osc = new Oscillator(pitch, phiList.get(pitch), envList.get(pitch));
                else
                    osc = new Oscillator(pitch, 0, 0);
                short[] tmpSamples = osc.getAudio();
                phiList.append(pitch, osc.getPhi());
                envList.append(pitch, osc.getEnv());
                for (int j = 0; j < UNIT_DURATION; j++)
                    mSamples[n+j] = (short) (mSamples[n+j] + tmpSamples[j]);
            }

            // 6- Update state.
            prevStateOn = pitches;
        }
    }

    /**
     * Initialize AudioTrack, write audio and reproduce it.
     */
    private void reproduce()
    {

        // 1- Eventual resource release.
        if (mAudioTrack != null)
        {
            mAudioTrack.release();
            mAudioTrack = null;
        }

        // 2- Audio player intialization.
        mAudioTrack = new AudioTrack.Builder()
                .setAudioAttributes(new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build())
                .setAudioFormat(new AudioFormat.Builder()
                        .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                        .setSampleRate(FRAME_RATE)
                        .setChannelMask(AudioFormat.CHANNEL_OUT_MONO)
                        .build())
                .setBufferSizeInBytes(2*LOOP*DURATION)
                .setTransferMode(AudioTrack.MODE_STATIC)
                .build();

        // 3- Write audio in the player.
        short[] loopSamples = new short[LOOP*DURATION];
        for (int i = 0; i < LOOP; i++)
            System.arraycopy(mSamples, 0, loopSamples, i * DURATION, DURATION);
        mAudioTrack.write(loopSamples, 0, LOOP*DURATION);

        // 4- Reproduce audio.
        mAudioTrack.play();
    }

    /**
     * Compute the position of non-zero elements in the vector.
     * @param vector Vector to analise.
     * @return Vector of notes to be played.
     */
    private Vector<Integer> getNotes(int[] vector)
    {
        Vector<Integer> notes = new Vector<>();

        for (int i = 0; i < vector.length; i++)
            if(vector[i] == 1)
                notes.add(i + MIN_NOTE);

        return notes;
    }

    /**
     * Private class for the implementation of the digital oscillator algorithm.
     */
    private class Oscillator
    {
        // Variables.
        private int pitch;                           // Pitch.
        private int phi;                             // Initial phase.
        private int env;                             // Initial index for the envelope.

        private short[] audio;                       // Vector for the waveform.

        // Constructor.
        Oscillator(int note, int initPhi, int initEnv)
        {
            pitch = note;
            phi = initPhi;
            env = initEnv;
            audio = new short[UNIT_DURATION];
        }

        // Synthesis method.
        short[] getAudio()
        {
            // 1- Get fundamental frequency and sampling increment.
            double exp = (double) (pitch - 69) / 12;
            double f0 = 440 * Math.pow(2, exp);
            double si = f0 * L / FRAME_RATE;

            // 2- Generate audio.
            for (int j = 0; j < UNIT_DURATION; j++)
            {
                short sample;
                if (env < mEnvelope.length)
                    sample = (short) (mEnvelope[env]*mTable[phi]);
                else
                    sample = 0;
                audio[j] = sample;

                phi = (int) Math.round((phi + si) % L) % L;
                env++;
            }

            return audio;
        }

        int getPhi()
        {   return phi;  }

        int getEnv()
        {   return env;  }
    }
}