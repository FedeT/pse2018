/*
 * @file Generator.java
 * @brief This class will generate the samples with the model based on the type of gestures detected.
 *
 * @author Federico Turrin (<federico.turrin@studenti.unipd.it>)
 * @author Marco Tramarin (<marco.tramarin@studenti.unipd.it>)
 * @author Alessandro Pomes (<alessandro.pomes@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2016-2017 University of Padua, Italy
 * @copyright Apache License, Version 2.0
 */

package com.pse2018.ibugs.muswipe;

import java.util.ArrayList;
import org.tensorflow.contrib.android.TensorFlowInferenceInterface;
import android.content.Context;

public class Generator
{
    // Constants.
    private static final String INPUT_NODE = "input";
    private static final String OUTPUT_NODE = "output";

    private static final String MODEL_ROCK = "file:///android_asset/Rock1_0008.pb";    // Rock model path.
    private static final String MODEL_POP = "file:///android_asset/Pop1_01.pb";   // Pop model path.


    private static final int modelDim = 1152;   //Dimension of trained network

    // Variables.
    private int choice = 0;                  // Model selection.

    private Context context;                 // Context of host activity.

    private int sample[];                    // Vector that contains the sample results.

    // Constuctor.
    Generator(ArrayList<Integer> gestures, Context cont)
    {
        // Context definition.
        this.context = cont;

        // Musical genre decision based on the user's gestures
        for(int i = 0; i < gestures.size(); i++)
            choice += gestures.get(i);
    }

    /**
     * Generate the samples with the network.
     */
    public void generate()
    {
        // Selected model path.
        String model;

        // Case empty or equal number: random choice
        if(choice == 0)
        {
            if (Math.round(Math.random()) == 0)
                choice = -1;
            else
                choice = 1;
        }

        // Music genre selection.
        if(choice > 0)
            model = MODEL_ROCK;
        else
            model = MODEL_POP;

        float[] inputFloats = new float[modelDim];               // Input vector.
        float[] resu = new float[modelDim];                      // Results vector.

        // Model loading.
        TensorFlowInferenceInterface inferenceInterface = new TensorFlowInferenceInterface(context.getAssets(), model);

        // Feed the network with the input vector.
        inferenceInterface.feed(INPUT_NODE, inputFloats,1 ,modelDim);

        // Compute the output.
        inferenceInterface.run(new String[]{OUTPUT_NODE}, false);

        // Copy the output node of the network in the 2nd parameter.
        inferenceInterface.fetch(OUTPUT_NODE, resu);

        sample = sampling(resu);
    }

    /**
     * Perform final step of Gibbs sampling method.
     * @param vet Probability vector of the network.
     * @return The sampled song.
     */
    private int[] sampling(float[] vet)
    {
        // Vectors declaration.
        float supp[] = new float[vet.length];
        int fin[] = new int[vet.length];

        // Generate uniform probabilty.
        for(int i = 0; i < vet.length; i++)
            supp[i] = (float) Math.random();

        // Add uniform probability to probability vector of the network.
        for(int i = 0; i < vet.length; i++)
        {
            fin[i] = (int) Math.floor((double) supp[i] + (double) vet[i]);
        }
        return fin;
    }

    /**
     * Return samples vector.
     * @return The samples vector.
     */
    public int[] getSamples()
    {   return sample;  }
}
