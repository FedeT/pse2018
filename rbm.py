import numpy as np
import glob
from tensorflow.python.ops import control_flow_ops
from tqdm import tqdm
import tensorflow as tf
import midi_manipulation




########### WARNING: this implemanatation can take a lot of time depending on which params and Dataset you chose.
########### Please in order to avoid crashes or wait too long check Pre-Processing Part or HyperParameters




#####Pre-Processing Part. Taking files .midi from a specified directory files are our Training-Datasets

def get_songs(path):
    files = glob.glob('{}/*.mid*'.format(path))
    songs = []
    for f in tqdm(files):
        try:
            song = np.array(midi_manipulation.midiToNoteStateMatrix(f))
            if np.array(song).shape[0] > 50:
                songs.append(song)
        except Exception as e:
            raise e
    return songs

songs = get_songs('Rock1') #Put here the name of the chosen Dataset
print ("{} songs processed".format(len(songs)))






#####HyperParameters

lowest_note = midi_manipulation.lowerBound #the index of the lowest note on the piano roll
highest_note = midi_manipulation.upperBound #the index of the highest note on the piano roll
note_range = highest_note-lowest_note #the note range

num_timesteps  = 32 #This is the number of timesteps that we will create at a time
n_visible   = 2*note_range*num_timesteps #This is the size of the visible layer.
n_hidden = 50 #This is the size of the hidden layer
num_epochs = 50 #The number of training epochs that we are going to run. For each epoch we go through the entire data set.
batch_size = 100 #The number of training examples that we are going to send through the RBM at a time.
lr = tf.constant(0.005, tf.float32) #The learning rate of our model

print(n_visible)


#####Variables:

x  = tf.placeholder(tf.float32, [None, n_visible], name="input") #The placeholder variable that holds our data
W  = tf.Variable(tf.random_normal([n_visible, n_hidden], 0.01), name="W") #The weight matrix that stores the edge weights
b = tf.Variable(tf.zeros([1, n_hidden],  tf.float32, name="b")) #The bias vector for the hidden layer
c = tf.Variable(tf.zeros([1, n_visible],  tf.float32, name="c")) #The bias vector for the visible layer

testSong=np.zeros((1,n_visible))






##### GIBBS ALGORITHM IMPLEMENTATIONS

def sample(probs):

    return tf.floor(probs + tf.random_uniform(tf.shape(probs), 0, 1))

#This function runs the gibbs chain. We will call this function in two places:
#    - When we define the training update step
#    - When we sample our music segments from the trained RBM
def gibbs_sample(k):
    #Runs a k-step gibbs chain to sample from the probability distribution of the RBM defined by W, bh, bv
    def gibbs_step(count, k, xk):
        #Runs a single gibbs step. The visible values are initialized to xk
        hk = sample(tf.sigmoid(tf.matmul(xk, W) + b)) #Propagate the visible values to sample the hidden values
        xk = sample(tf.sigmoid(tf.matmul(hk, tf.transpose(W)) + c)) #Propagate the hidden values to sample the visible values
        return count+1, k, xk

    #Run gibbs steps for k iterations
    ct = tf.constant(0) #counter
    [_, _, x_sample] = control_flow_ops.while_loop(lambda count, num_iter, *args: count < num_iter,
                                         gibbs_step, [ct, tf.constant(k), x])
    #This is not strictly necessary in this implementation, but if you want to adapt this code to use one of TensorFlow's
    #optimizers, you need this in order to stop tensorflow from propagating gradients back through the gibbs step
    x_sample = tf.stop_gradient(x_sample)
    return x_sample






##### Training Update Variable W,b,c. Implementations of contrastive divergence algorithm.

x_sample = gibbs_sample(1)
#The sample of the hidden nodes, starting from the visible state of x
h = sample(tf.sigmoid(tf.matmul(x, W) + b))
#The sample of the hidden nodes, starting from the visible state of x_sample
h_sample = sample(tf.sigmoid(tf.matmul(x_sample, W) + b))
#Next, we update the values of W, bh, and bv, based on the difference between the samples that we drew and the original values
size_bt = tf.cast(tf.shape(x)[0], tf.float32)
W_adder  = tf.multiply(lr/size_bt, tf.subtract(tf.matmul(tf.transpose(x), h), tf.matmul(tf.transpose(x_sample), h_sample)))
bv_adder = tf.multiply(lr/size_bt, tf.reduce_sum(tf.subtract(x, x_sample), 0, True))
bh_adder = tf.multiply(lr/size_bt, tf.reduce_sum(tf.subtract(h, h_sample), 0, True))
#When we do sess.run(updt), TensorFlow will run all 3 update steps
updt = [W.assign_add(W_adder), c.assign_add(bv_adder), b.assign_add(bh_adder)]






###### TRAINING PHASE

with tf.Session() as sess:
    #First, we train the model
    #initialize the variables of the model
    init = tf.global_variables_initializer()
    sess.run(init)
    #Run through all of the training data num_epochs times
    for epoch in tqdm(range(num_epochs)):
        for song in songs:
            #The songs are stored in a time x notes format. The size of each song is timesteps_in_song x 2*note_range
            #Here we reshape the songs so that each training example is a vector with num_timesteps x 2*note_range elements
            song = np.array(song)
            song = song[:int(np.floor(song.shape[0]/num_timesteps)*num_timesteps)]
            song = np.reshape(song, [song.shape[0]//num_timesteps, song.shape[1]*num_timesteps])
            #Train the RBM on batch_size examples at a time
            for i in range(1, len(song), batch_size):
                tr_x = song[i:i+batch_size]
                sess.run(updt, feed_dict={x: tr_x})
   #Now the  Model has reached the parmas.
   #In Order to import the model in Android the params are saved
   #and converted to constant
    WC1 = W.eval(sess)
    B = b.eval(sess)
    C = c.eval(sess)






##### Define a new default Graph. This Graph is a branch of the full Graph
#it represent the last part of the process: The last Gibbs step.

g = tf.Graph()
with g.as_default():
    #Define a new Tensor for the input of the graph
    #This Tensor will be fed with the 1-D vector in Class Generator (INPUT_NODE)
    #It is important to put the same label name "input" that is the one used
    #when TensorflowInferenceInterface is called
     x_2 = tf.placeholder("float", shape=[None, n_visible], name="input")
     #Start with the first sample of Gibbs algorithm founding h^(i+1)~p(h|v^(i))
     H_visible= sample(tf.sigmoid(tf.matmul(x_2, WC1) + B))
     #Move the values of found of h^(i+1) in the visible layer.
     #The sample final operation will be made in Generator Class
     output=sample(tf.sigmoid(tf.matmul(H_visible, tf.transpose(WC1)) + C, name="output"))
     #Define another session
     sess1 = tf.Session()
     #inizialize all the Tensor defined
     init = tf.global_variables_initializer()
     #Run the initialization
     sess1.run(init)
     #Declare g like default draph
     graph_def = g.as_graph_def()
     #Save Graph g in a precise path and with a precise name
     tf.train.write_graph(graph_def,'.', 'Rock1.pb', as_text=False)



    ##If some files midi generation is needed please de-comment these lines
     # for j in range(10):
     #     sample = sess1.run(output, feed_dict={x_2: np.zeros((1, n_visible))})
     #     for i in range(sample.shape[0]):
     #         if not any(sample[i, :]):
     #                 continue
     #             # Here we reshape the vector to be time x notes, and then save the vector as a midi file
     #         S = np.reshape(sample[i, :], (num_timesteps, 2 * note_range))
     #         pop1="Rock1_0008_"+str(j)
     #     midi_manipulation.noteStateMatrixToMidi(S, pop1.format(i))









